namespace :doorkeeper do
  desc 'Creates an OAuth2 client for Doorkeeper'
  task create_client: :environment do
    # check the variables
    app_name = Rails.application.secrets[:app_name]
    app_uri = Rails.application.secrets[:app_uri]

    if app_name.blank? || app_uri.blank?
      puts 'Usage: rake doorkeeper:create_client NAME=name REDIRECT_URI=redirect_uri'
      exit
    end

    # create the application
    @application = Doorkeeper::Application.new(name: app_name, redirect_uri: app_uri)
    if @application.save
      puts "Created Doorkeeper OAuth2 client with (name: #{@application.name}, redirect_uri: #{@application
                                                                                                 .redirect_uri})"
      puts "client_id: #{@application.uid}"
      puts "client_secret: #{@application.secret}"
    else
      puts @application.errors.full_messages
    end
  end
end
