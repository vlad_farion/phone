namespace :user do
  desc 'import bots'
  namespace :import do
    desc 'import male'
    task male: :environment do
      VkFileJob.perform_later 'male'
    end
    desc 'import female'
    task female: :environment do
      VkFileJob.perform_later 'female'
    end
  end

  desc 'import geo data'
  namespace :location do
    desc 'import data'
    task import: :environment do
      GeoDatum.import
    end
    desc 'update location'
    task update: :environment do
      GeoDatum.upd_location
    end
  end
end
