# README
Things you may want to cover:

* Ruby version 2.4.1, Rails 5.1.4

* System dependencies

* Configuration

* Database creation

* Database initialization
```
rake doorkeeper:create_client           # create application, get id and secret from echo
rake db:seed:init                       # first uset
rake user:import:female                 # import female
rake user:import:male                   # import male
rake user:location:import               # import data
rake user:location:update               # update location
```

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
response = HTTParty.get("https://www.googleapis.com/oauth2/v2/userinfo", headers: {"Access_token"  => ttn,"Authorization" => ttn})
client_id: ad18285def1a45a153fea96e3e7ae087adc2a745fbf48cd824486298540c3cf0
client_secret: f336eda0f8fb220cd56586db57ad425b1b21eb0ae0f6b6b4af9a79e620ccb157
cap staging invoke:rake TASK=user:import:male  
stup twilio https://rails.devcamp.com/rails-bdd-tdd-course/twilio-api-text-messages/creating-rspec-stub-sending-sms-messages
