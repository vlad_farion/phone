# app = Doorkeeper::Application.create!(:name => 'phone', :redirect_uri => 'phone://oauth/callback')
app = Doorkeeper::Application.last
# password = Devise.friendly_token[0..10]
password = 'foobar'
user = User.new(email: "#{SecureRandom.hex(8)}@example.org", password: password, password_confirmation: password)
user.save
user.doorkeeper_auth(app.id)
user.find_or_create_geo!(latitude: 48.28179,  longitude: 25.93257)
