class CreateSympathies < ActiveRecord::Migration[5.1]
  def change
    create_table :sympathies do |t|
      t.references :user, foreign_key: true
      t.integer          :follower_id, index: true
      t.boolean          :accepted

      t.timestamps
    end
    add_index :sympathies, [:user_id, :follower_id]
  end
end
