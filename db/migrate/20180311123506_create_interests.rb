class CreateInterests < ActiveRecord::Migration[5.1]
  def change
    create_table :interests do |t|
      t.string :name, limit: 100
      t.timestamps
    end

    create_table :interests_user_profiles do |t|
      t.belongs_to :interest, index: true
      t.belongs_to :user_profile, index: true
    end
  end
end
