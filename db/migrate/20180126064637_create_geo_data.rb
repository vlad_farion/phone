class CreateGeoData < ActiveRecord::Migration[5.1]
  def change
    create_table :geo_data do |t|
      t.references :user, foreign_key: true
      t.decimal :latitude, precision: 10, scale: 6, null: false, default: 0
      t.decimal :longitude, precision: 10, scale: 6, null: false, default: 0

      t.timestamps
    end
    add_index :geo_data, [:latitude, :longitude]
  end
end
