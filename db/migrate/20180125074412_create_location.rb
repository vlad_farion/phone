class CreateLocation < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.references :user, foreign_key: true
      t.decimal :latitude, precision: 10, scale: 6, null: false, default: 0
      t.decimal :longitude, precision: 10, scale: 6, null: false, default: 0

      t.timestamps
    end
    add_index :locations, [:latitude, :longitude]
  end
end
