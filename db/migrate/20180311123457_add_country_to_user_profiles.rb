class AddCountryToUserProfiles < ActiveRecord::Migration[5.1]
  def change
    add_column :user_profiles, :country, :string, limit: 55
  end
end
