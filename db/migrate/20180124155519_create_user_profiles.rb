class CreateUserProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :user_profiles do |t|
      t.references :user, foreign_key: true
      t.string    :name,      null: false, default: ''
      t.integer   :gender,    null: false, default: 0
      t.date      :birthday
      t.string    :city,      null: false, default: ''
      t.integer   :sexual,    null: false, default: 0

      t.timestamps
    end
  end
end
