require 'rails_helper'
require_relative 'api_helper'

describe 'Sympathy API' do
  include_context 'users'
  let(:access_token) { create(:access_token, resource_owner_id: user.id) }

  describe 'GET #index' do
    let(:http_method) { :get }
    let(:url) { api_v1_sympathies_path }
    let!(:users) { create_list(:user, 3) }
    let!(:user_geo) { create(:geo_datum, user_id: user.id) }

    let!(:sexualy) do
      users.each { |u| u.user_profile.update(sexual: user.user_profile.sexual) }
    end

    let!(:photos) do
      users.each { |u| create(:photo, user_id: u.id) }
    end

    let!(:location) do
      users.each do |u|
        create(:geo_datum, latitude: user.location.latitude, longitude: user.location.longitude, user_id: u.id)
      end
    end

    let(:params) { { access_token: access_token.token } }

    subject { do_request(url, http_method, params) }

    it_behaves_like 'unauthorized'
    context 'authorized' do
      describe 'if user is not to long away' do
        before { subject }
        it { expect(response.body).to have_json_size(3) }
      end
      describe 'if user to long away' do
        let!(:new_geo) { create(:geo_datum, user_id: users.last.id, latitude: 40.71427, longitude: -74.00597) }
        before { subject }
        it { expect(response.body).to have_json_size(2) }
      end
    end
  end

  describe 'POST #create' do
    let(:http_method) { :post }
    let(:url) { api_v1_sympathies_path }
    let(:params) do
      {
        access_token: access_token.token,
        follower_id: john.id
      }
    end

    subject { do_request(url, http_method, params) }

    it_behaves_like 'unauthorized'

    context 'authorized' do
      describe 'when not have sympathy before' do
        it { expect { subject }.to change(Sympathy, :count).by(1) }
      end

      describe 'when not have sympathy before' do
        let!(:sympathy) { create(:sympathy, user_id: user.id, follower_id: john.id) }
        before do
          subject
          sympathy.reload
        end

        it { expect(sympathy.accepted).to be_truthy }
      end
    end
  end
end
