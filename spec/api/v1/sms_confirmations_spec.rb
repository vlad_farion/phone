require 'rails_helper'
require_relative 'api_helper'

describe 'SMS Confirmations API' do
  include ActiveJob::TestHelper
  include_context 'users'
  let(:application) { create(:oauth_application) }
  let!(:access_token) { create(:access_token, application_id: application.id, resource_owner_id: user.id) }

  describe 'PATCH /update' do
    let(:http_method) { :patch }
    let(:url) { api_v1_sms_confirmations_path }
    let(:profile_params) { attributes_for(:user_profile, name: '1235') }
    let(:code) { SmsConfirmable::CodeGenerator.generate }

    let(:params) do
      {
        access_token: access_token.token,
        confirmation_code: code
      }
    end

    it_behaves_like 'unauthorized'

    describe 'user authorised' do
      let(:form_params) { { name: '11' } }
      subject do
        do_request(url, http_method, params)
      end

      describe 'if code wrong' do
        before { subject }
        it { expect(response.status).to eq 401 }
        it do
          expect(response.body).to be_json_eql(I18n.t('confirmation_token.errors.no_valid').to_json)
            .at_path('error')
        end
      end

      describe 'if code right' do
        before do
          user.confirmation_token = code
          user.save
          user.reload
          subject
        end

        it { expect(response.status).to eq 201 }
        it do
          expect(response.body).to be_json_eql(I18n.t('confirmation_token.success.confirmed').to_json)
            .at_path('detail')
        end
      end
    end
  end
  describe 'POST /create' do
    let(:http_method) { :post }
    let(:url) { api_v1_sms_confirmations_path }
    let(:profile_params) { attributes_for(:user_profile, name: '1235') }
    let!(:authorization) { create(:authorization, user_id: user.id, provider: 'doorkeeper', uid: '380503745090') }
    let(:params) { { access_token: access_token.token } }

    subject do
      do_request(url, http_method, params)
    end

    it_behaves_like 'unauthorized'

    describe 'user authorised', perform_enqueued: true do
      it do
        expect(SmsConfirmationJob).to receive(:perform_later).with(user)
        subject
      end
      it do
        subject
        expect(response.body).to be_json_eql(I18n.t('confirmation_token.success.sended').to_json)
          .at_path('detail')
      end
    end
  end
end
