require 'rails_helper'
require_relative 'api_helper'

describe 'geo datum API' do
  include_context 'users'
  let(:access_token) { create(:access_token, resource_owner_id: user.id) }

  describe 'POST #create' do
    let(:http_method) { :post }
    let(:url) { api_v1_geo_datums_path }
    let(:form_params) { {} }

    it_behaves_like 'unauthorized'

    context 'authorized' do
      let(:params) do
        {
          access_token: access_token.token,
          geo_data: api_normalize(attributes_for(:geo_datum, user_id: user.id))
        }.merge(form_params)
      end

      context 'valid data' do
        before do
          do_request(url, http_method, params)
        end

        it { expect(response.status).to eq 201 }

        it_behaves_like 'success response'
      end

      context 'invalid data' do
        let!(:form_params) { { geo_data: { latitude: nil } } }

        before do
          do_request(url, http_method, params)
        end

        it 'returns 422 status code' do
          expect(response.status).to eq 422
        end

        it 'returns errors' do
          expect(response.body).to have_json_path('errors')
        end
      end
    end
  end

  describe 'GET #index' do
    let(:http_method) { :get }
    let(:url) { api_v1_geo_datums_path }
    let!(:geo) { create_list(:geo_datum, 2, user_id: user.id) }
    let!(:other_geo) { create_list(:geo_datum, 2, user_id: john.id) }

    it_behaves_like 'unauthorized'

    context 'authorized' do
      before do
        do_request(url, http_method, access_token: access_token.token)
      end

      it_behaves_like 'success response'

      it 'contains geo data' do
        expect(response.body).to have_json_size(2)
      end
    end
  end
end
