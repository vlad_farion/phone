require 'rails_helper'
require_relative 'api_helper'

describe 'Photos API' do
  include_context 'users'
  let(:access_token) { create(:access_token, resource_owner_id: user.id) }

  describe 'GET #index' do
    let(:http_method) { :get }
    let(:url) { api_v1_photos_path }
    let!(:photos) { create_list(:photo, 2, user_id: user.id) }
    let!(:other_photos) { create_list(:photo, 2, user_id: john.id) }

    it_behaves_like 'unauthorized'

    context 'authorized' do
      before do
        do_request(url, http_method, access_token: access_token.token)
      end

      it_behaves_like 'success response'

      it 'contains photos url' do
        expect(response.body).to have_json_size(2)
      end
    end
  end

  describe 'GET #show' do
    let(:http_method) { :get }
    let(:photo) { create(:photo, user_id: user.id) }
    let(:url) { "/api/v1/photos/#{photo.id}" }

    it_behaves_like 'unauthorized'

    context 'authorized' do
      before do
        do_request(url, http_method, access_token: access_token.token)
      end

      it_behaves_like 'success response'

      %w(id user_id image_medium image_thumb).each do |attr|
        it "photo contains #{attr}" do
          expect(response.body).to have_json_path(attr.camelize(:lower))
        end
      end
    end
  end

  describe 'POST #create' do
    let(:http_method) { :post }
    let(:url) { api_v1_photos_path }

    it_behaves_like 'unauthorized'

    context 'authorized' do
      context 'valid data' do
        let(:params) do
          {
            access_token: access_token.token,
            photo: api_normalize(attributes_for(:photo))
          }
        end

        before do
          do_request(url, http_method, params)
        end

        it { expect(response.status).to eq 201 }

        it_behaves_like 'success response'
      end

      context 'invalid data' do
        let(:params) do
          {
            access_token: access_token.token,
            photo: { image: nil }
          }
        end

        before do
          do_request(url, http_method, params)
        end

        it 'returns 422 status code' do
          expect(response.status).to eq 422
        end

        it 'returns errors' do
          expect(response.body).to have_json_path('errors')
        end
      end
    end
  end

  describe 'DELETE /destroy' do
    let(:http_method) { :delete }
    let!(:photo) { create(:photo, user_id: user.id) }
    let(:url) { "/api/v1/photos/#{photo.id}" }
    let(:params) { { access_token: access_token.token } }

    it_behaves_like 'unauthorized'

    context 'authorized' do
      subject { do_request(url, http_method, params) }

      describe 'user authorised' do
        it { expect(subject).to eq 204 }
        it { expect { subject }.to change(Photo, :count).by(-1) }
      end
    end
  end
end
