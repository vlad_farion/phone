require 'rails_helper'
require_relative 'api_helper'

describe 'Profile API' do
  allowed_attributes = %w(id)
  include_context 'users'

  describe 'GET /me' do
    let(:http_method) { :get }
    let(:url) { api_v1_me_path }

    it_behaves_like 'unauthorized', 'me'

    context 'authorized' do
      let(:access_token) { create(:access_token, resource_owner_id: user.id) }

      before do
        do_request(url, http_method, access_token: access_token.token)
      end

      it_behaves_like 'success response'

      allowed_attributes.each do |attr|
        it "returns user #{attr}" do
          expect(response.body).to be_json_eql(user.send(attr.underscore.to_sym).to_json).at_path(attr)
        end
      end
    end
  end

  describe 'POST /create' do
    let(:application) { create(:oauth_application) }
    let(:form_params) { {} }
    let(:provider) { {} }
    let(:user_params) { attributes_for(:user) }
    let(:http_method) { :post }
    let(:params) do
      {
        uid: application.uid,
        secret: application.secret,
        user: user_params
      }.merge(form_params).merge(provider)
    end

    describe 'application unauthorised' do
      let(:form_params) { { uid: '', secret: '' } }
      let(:provider) { { provider: 'doorkeeper' } }
      subject do
        do_request(api_v1_profiles_path, http_method, params)
      end

      it { expect(subject).to eq 401 }

      describe 'return error message' do
        before { subject }
        it do
          expect(response.body)
            .to be_json_eql(t('api.errors.no_app').to_json).at_path('detail')
        end
      end
    end

    describe 'application authorised' do
      subject do
        do_request(api_v1_profiles_path, http_method, params)
      end

      context 'when user attributes right' do
        let(:provider) { { provider: { name: 'doorkeeper', token: '123' } } }

        it 'return success' do
          expect(subject).to eq 201
        end
        it { expect { subject }.to change(User, :count).by(1) }
        describe 'return access token' do
          before { subject }

          it do
            expect(response.body)
              .to be_json_eql(Doorkeeper::AccessToken.find_by_resource_owner_id(User.last.id).token.to_json)
              .at_path('access_token')
          end
        end
      end

      context 'when provider attributes wrong' do
        let(:provider) { { provider: { name: 'doorkeeper', token: '' } } }

        subject do
          do_request(api_v1_profiles_path, http_method, params)
        end

        it { expect(subject).to eq 422 }
      end

      context 'when user already present' do
        let!(:authorization) {  create(:authorization, provider: 'doorkeeper', uid: 123.to_s, user_id: user.id) }
        let!(:provider) { { provider: { name: 'doorkeeper', token: '123' } } }
        before { subject }

        it 'return success' do
          expect(subject).to eq 201
        end

        it { expect { subject }.to_not change(User, :count) }

        it do
          expect(response.body)
            .to be_json_eql(Doorkeeper::AccessToken.find_by_resource_owner_id(user.id).token.to_json)
            .at_path('access_token')
        end
      end
    end
  end
end
