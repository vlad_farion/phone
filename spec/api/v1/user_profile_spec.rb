require 'rails_helper'
require_relative 'api_helper'

describe 'User Profile API' do
  include_context 'users'
  let(:application) { create(:oauth_application) }
  let!(:access_token) { create(:access_token, application_id: application.id, resource_owner_id: user.id) }

  describe 'PATCH /update' do
    let(:http_method) { :patch }
    let(:url) { api_v1_user_profiles_path }
    let(:profile_params) { attributes_for(:user_profile, name: '1235') }
    let(:form_params) { {} }

    let(:params) do
      {
        access_token: access_token.token,
        user: api_normalize(profile_params.merge(form_params))
      }
    end

    it_behaves_like 'unauthorized'

    # describe 'user authorised' do
    #   let(:form_params) { { name: '11' } }
    #   subject do
    #     do_request(url, http_method, params)
    #     user.reload
    #   end
    #   before { subject }
    #   it { expect(response.status).to eq 200 }
    #   it { expect(user.user_profile.name).to eql form_params[:name] }
    # end
  end

  describe 'GET /show' do
    let(:http_method) { :get }
    let(:url) { api_v1_user_profiles_path }

    let(:params) do
      {
        access_token: access_token.token,
        user_id: user.id
      }
    end

    it_behaves_like 'unauthorized'

    context 'authorized' do
      before do
        do_request(url, http_method, params)
      end

      it_behaves_like 'success response'
    end
  end
end
