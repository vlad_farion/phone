require 'rubygems'
require 'database_cleaner'
require 'webmock/rspec'
require 'paperclip/matchers'

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
# require 'devise'

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
Dir[Rails.root.join('spec/shared_contexts/**/*.rb')].each { |f| require f }
Dir[Rails.root.join('spec/shared_examples/**/*.rb')].each { |f| require f }
Dir[Rails.root.join('spec/support/macros/*.rb')].each { |f| require f }

WebMock.disable_net_connect!(allow_localhost: true)
ActiveJob::Base.queue_adapter = :test

RSpec.configure do |config|
  config.include AbstractController::Translation
  config.include Rails.application.routes.url_helpers

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.after(:suite) do
    FileUtils.rm_rf(Dir["#{Rails.root}/tmp/paperclip/"])
  end
  config.include Paperclip::Shoulda::Matchers
  config.before(:each) do
    stub_request(:get, /admin.paysale.com/).to_return(body: 'OK', status: 200)
    stub_request(:post, /api.twilio.com/)
      .with(
        body: { 'Body' => /\d\d\d\d\d\d/, 'From' => '+15005550006', 'To' => '+380503745090' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Charset' => 'utf-8',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Authorization' =>
            'Basic QUMxY2ZkYzZjN2Q2ZTFhOWNiYzg3NDljNDZkY2JlZGQ1OTo0ZTg1MzEyMzg0NThiMGNiOTg3OGU4ZjFiNDdjNThjYg==',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'twilio-ruby/5.6.2 (ruby/x86_64-linux 2.4.1-p111)'
        }
      ).to_return(status: 200, body: '', headers: {})
  end
end
