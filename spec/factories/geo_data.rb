FactoryBot.define do
  factory :geo_datum do
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
  end
end
