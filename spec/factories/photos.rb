FactoryBot.define do
  factory :photo do
    image do
      Rack::Test::UploadedFile
        .new("#{Rails.root}/spec/support/for_upload/no_picture.png", 'image/png')
    end
  end
end
