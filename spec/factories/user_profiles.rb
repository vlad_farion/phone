FactoryBot.define do
  factory :user_profile do
    name { Faker::FamilyGuy.character }
    gender %i(male female).sample
    sexual %i(hetero homo bisex).sample
    birthday Faker::Date.birthday(18, 65)
  end
end
