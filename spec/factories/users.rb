FactoryBot.define do
  sequence :email do |n|
    "user#{n}@example.com"
  end

  factory :user do
    email
    password 'a' * 8
    # avatar { File.new("#{Rails.root}/spec/support/for_upload/noavatar.png") }
    confirmed_at Time.now

    after(:create) do |user|
      create(:user_profile, user_id: user.id)
    end
  end
end
