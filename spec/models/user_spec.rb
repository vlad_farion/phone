require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:photos).dependent(:destroy) }
  it { should have_many(:geo_datum).dependent(:destroy) }
  it { should have_one(:user_profile).dependent(:destroy) }
  it { should have_one(:location).dependent(:destroy) }
  it { should have_many(:sympathies) }
end
