require 'rails_helper'

RSpec.describe Photo, type: :model do
  it { should belong_to(:user) }
  it do
    should validate_attachment_content_type(:image)
      .allowing('image/png', 'image/gif', 'image/jpg', 'image/jpeg')
  end
end
