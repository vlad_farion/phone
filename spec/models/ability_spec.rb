require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Ability, type: :model do
  subject(:ability) { Ability.new(user) }

  describe 'for guest' do
    let(:user) { nil }

    it { should be_able_to :create, User }
    it { should_not be_able_to :read, :all }
  end

  describe 'for user' do
    include_context 'users'

    let(:user_location) { create(:location, user_id: user.id) }
    let(:john_location) { create(:location, user_id: john.id) }
    let(:user_photo) { create(:photo, user_id: user.id) }
    let(:john_photo) { create(:photo, user_id: john.id) }
    let!(:user_geo) { create(:geo_datum, user_id: user.id) }
    let!(:john_geo) { create(:geo_datum, user_id: john.id) }

    #   it { should be_able_to :read, :all }
    it { should_not be_able_to :manage, :all }
    it { should be_able_to :create, Photo }
    it { should be_able_to :create, Location }
    it { should be_able_to :create, GeoDatum }
    it { should be_able_to :create, UserProfile }

    context 'read' do
      it { should be_able_to :read, user_location }
      it { should be_able_to :read, user_geo }
      it { should_not be_able_to :read, john_location }
      it { should_not be_able_to :read, john_geo }
    end

    context 'update' do
      it { should be_able_to :update, user_location }
      it { should be_able_to :update, user_photo }
      it { should be_able_to :update, user.user_profile }
      it { should_not be_able_to :update, john_location }
      it { should_not be_able_to :update, john_photo }
      it { should_not be_able_to :update, john.user_profile }
    end

    context 'destroy' do
      it { should be_able_to :destroy, user_photo }
      it { should be_able_to :destroy, user.user_profile }
      it { should be_able_to :destroy, user_geo }
      it { should_not be_able_to :destroy, john_photo }
      it { should_not be_able_to :destroy, john.user_profile }
      it { should_not be_able_to :destroy, john_geo }
    end

    it { should be_able_to :me, User }
  end
end
