require 'rails_helper'

RSpec.describe UserProfile, type: :model do
  it { should belong_to(:user) }
  it do
    should define_enum_for(:gender).with(not_specified: 0, female: 1, male: 2)
  end
  it do
    should define_enum_for(:sexual).with(unknown: 0, hetero: 1, homo: 2, bisex: 3)
  end
end
