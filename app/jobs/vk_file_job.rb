require 'logger'

class VkFileJob < ApplicationJob
  # self.queue_adapter = :sidekiq
  queue_as :default

  def perform(gender)
    logger = Logger.new("#{Rails.root}/log/vk_import_job.log")
    logger.datetime_format = '%d-%m-%Y %H:%M:%S'
    logger.info '*!!! Start job }'
    file = if gender == 'male'
             File.read("#{Rails.root}/db/seeds/male.json")
           elsif gender == 'female'
             File.read("#{Rails.root}/db/seeds/female.json")
           end
    import(logger, file)
  end

  def import(logger, file)
    parsed_time = Time.current
    parsed_data = JSON.parse(file)
    if parsed_data.present?
      logger.info '*!!! Request from VK is OK'
      import_count = 2000
      import_separate(parsed_data, logger, import_count) # Import female
      logger.info "*!!! Import from VK complete, all imported: #{parsed_time} #{parsed_data.count} users"
    else
      logger.warn '*!!! Request is filed!!!!!'
      logger.debug "*!!! #{parsed_data}"
    end
  rescue StandardError => e
    raise e
  end

  def import_separate(parsed_data, logger, import_count)
    logger.info '*!!! Start parse:'
    n = 1
    parsed_data.each do |result|
      logger.info "*!!! Start parse: #{result}"
      password = 'foobar'

      gender = if result['sex'] == 1
                 'female'
               elsif result['sex'] == 2
                 'male'
               end
      next if result['ava'].nil?
      user = User.new(email: result['email'],
                      password: password,
                      password_confirmation: password)
      if user.valid?
        user.save!
        user.user_profile.update_attributes!(
          name: result['first_name'],
          birthday: result['age'].to_date,
          gender: gender,
          sexual: %w(hetero homo bisex).sample
        )
        logger.info '*!!! User saved'
      else
        logger.info '*!!! User invalid'
        next
      end

      if user.persisted?
        user.authorizations.create!(provider: 'doorkeeper', uid: result['uid'].to_s)
        user.doorkeeper_auth(Doorkeeper::Application.last.id)
        logger.info '*!!! Authorization record created'
        result['photos'].each do |p|
          user.photos.create(image: URI.parse(p))
          logger.info "*!!! User photos added #{p}"
        end
        logger.info '*!!! User photos added'
      end
      n += 1
      break if n >= import_count
    end
  end
end
