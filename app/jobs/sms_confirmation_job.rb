class SmsConfirmationJob < ApplicationJob
  queue_as :sms

  def perform(user)
    SmsConfirmable::ConfirmationSender.send_confirmation_to(user)
  end
end
