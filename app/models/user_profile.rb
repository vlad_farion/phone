class UserProfile < ApplicationRecord
  belongs_to :user

  enum gender: { not_specified: 0, female: 1, male: 2 }
  enum sexual: { unknown: 0, hetero: 1, homo: 2, bisex: 3 }

  scope :compose_gender, ->(hash) { where(gender: hash[:gender]).where(sexual: hash[:sexual]) }

  scope :gender_male, -> { where(gender: 'male') }
  scope :gender_female, -> { where(gender: 'female') }
  scope :gender_unknown, -> { where(gender: 'unknown') }

  validates :name, length: { maximum: 15 }

  has_and_belongs_to_many :interests

  def compose_bisex(gender)
    if gender == 'female'
      gender_male.where(sexual: %w(hetero bisex unknown))
                 .merge(gender_female.where(sexual: %w(bisex unknown homo))).merge(gender_unknown)
    elsif gender == 'male'
      gender_male.where(sexual: ['homo bisex unknown'])
                 .merge(gender_female.where(sexual: %w('bisex hetero unknown'))).merge(gender_unknown)
    end
  end
end
