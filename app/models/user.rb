class User < ApplicationRecord
  include UserGeoDates
  include OauthAuthorization

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :trackable, :validatable, :omniauthable, omniauth_providers: %i[facebook doorkeeper google_oauth2]

  has_many :authorizations, dependent: :destroy
  has_many :photos, dependent: :destroy
  has_one :token, -> { order 'created_at DESC' }, class_name: 'Doorkeeper::AccessToken',
                                                  foreign_key: :resource_owner_id
  has_one :user_profile, dependent: :destroy
  has_many :geo_datum, dependent: :destroy
  has_many :sympathies, dependent: :destroy
  has_many :received_sympathies, class_name: 'Sympathy', foreign_key: 'follower_id', dependent: :destroy

  has_many :active_likes, -> { where(sympathies: { accepted: true }) }, through: :sympathies, source: :follower
  has_many :received_likes, -> { where(sympathies: { accepted: true }) }, through: :sympathies, source: :user
  has_many :pending_likes, -> { where(sympathies: { accepted: false }) }, through: :sympathies, source: :follower
  # has_many :requested_sympathy, -> { where(sympathies: { accepted: false }) },
  #          through: :received_sympathy, source: :user

  accepts_nested_attributes_for :user_profile, update_only: true

  after_create_commit :create_profile

  scope :all_except, ->(user) { where.not(id: user.id).where(id: User.sexual(user).pluck(:user_id)) }

  scope :except_me, ->(user_id) { where.not(id: user_id) }

  scope :with_photos, -> { where('id IN (SELECT DISTINCT(user_id) FROM photos)') }

  scope :sexual, lambda { |user|
    UserProfile.where(sexual: [user.user_profile.sexual, 'unknown'])
  }

  scope :no_sympathies, lambda { |user|
    where('id NOT IN (SELECT DISTINCT(follower_id) FROM sympathies WHERE
            sympathies.user_id = :user_id OR sympathies.accepted = true)', user_id: user.id)
  }

  scope :witch_sympathies, lambda { |user|
    where('id IN (SELECT DISTINCT(follower_id) FROM sympathies WHERE
            sympathies.user_id = :user_id OR sympathies.follower_id = :user_id)', user_id: user.id)
  }

  scope :by_gender, ->(gender) { where(gender: gender) }

  def gender_select
    owner ||= self
    gender ||= owner.user_profile.gender
    opposite_gender ||= owner.opposite_gender
    sexual ||= owner.user_profile.sexual

    case sexual
    when 'hetero'
      UserProfile.compose_gender(sexual: %w[hetero bisex unknown], gender: opposite_gender)
    when 'homo'
      UserProfile.compose_gender(sexual: %w[homo bisex unknown], gender: gender)
    when 'bisex'
      UserProfile.compose_bisex(gender)
    else
      UserProfile.compose_gender(sexual: %w[hetero bisex unknown homo], gender: opposite_gender << gender)
    end
  end

  def opposite_gender
    case user_profile.gender
    when 'male'
      %w(female not_specified)
    when 'female'
      %w[male not_specified]
    else
      %w[female male not_specified]
    end
  end

  def create_profile
    UserProfile.create!(user_id: id)
  end

  def self.matches_list(user)
    except_me(user.id).where(id: user.gender_select.pluck(:user_id))
                      .sympathies_location(user, 10_000)
                      .with_photos.where.not(id: witch_sympathies(user).pluck(:id)).order('RANDOM()')
  end
end
