class Sympathy < ApplicationRecord
  belongs_to :user
  belongs_to :follower, class_name: 'User'

  validates_presence_of :user_id, :follower_id
  validates :user_id, uniqueness: { scope: :follower_id }

  scope :accepted_sympathies, lambda { |user|
    where(
      '(sympathies.user_id = :user_id OR sympathies.follower_id = :user_id)',
      user_id: user.id
    ).where(accepted: true).order('updated_at DESC')
  }

  scope :waiting_sympathies, lambda { |user|
    where(
      '(sympathies.user_id = :user_id OR sympathies.follower_id = :user_id)',
      user_id: user.id
    ).where(accepted: false).order('updated_at DESC')
  }

  scope :between, lambda { |user_id, follower_id|
    where(user_id: user_id, follower_id: follower_id)
      .or(where(user_id: follower_id, follower_id: user_id)).take
  }
end
