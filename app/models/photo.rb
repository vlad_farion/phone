class Photo < ApplicationRecord
  belongs_to :user

  has_attached_file :image,
                    styles: { medium: '70%', small: '40%', thumb: '200x200#' },
                    processors: %i[thumbnail compression]
  validates_attachment :image, presence: true,
                               content_type: { content_type: %w(image/jpg image/jpeg image/png image/gif) }
end
