require 'omniauth-oauth2'

module Strategies
  extend ActiveSupport::Concern
  class Doorkeeper < OmniAuth::Strategies::OAuth2
    include OmniAuth::Strategy
    option :client_options,
           site: 'http://myapp.loc',
           authorize_url: 'http://myapp.loc/oauth/authorize',
           token_url: 'http://myapp.loc/oauth/token'

    def request_phase
      super
    end

    info do
      raw_info.merge('token' => access_token.token)
    end
    uid { raw_info['id'] }

    def raw_info
      @raw_info ||=
        access_token.get('/api/users/me').parsed
    end
  end
end
