module ImportGeo
  extend ActiveSupport::Concern
  included do
    def self.import
      GeoDatum.connection.execute <<-SQL
      CREATE TEMP TABLE imports
        (
          Latitude DECIMAL,
          Longitude DECIMAL
        )
      SQL

      File.open("#{Rails.root}/db/seeds/gps.csv") do |file|
        GeoDatum
          .connection
          .raw_connection
          .copy_data %{copy imports from stdin with(FORMAT CSV, HEADER TRUE, delimiter ',', quote '"')} do
          while (line = file.gets)
            GeoDatum.connection.raw_connection.put_copy_data line
          end
        end
      end
      User.all.map do |user|
        GeoDatum.connection.execute <<-SQL
          INSERT INTO geo_data(user_id, latitude, longitude, created_at, updated_at)
          SELECT #{user.id} AS user_id, Latitude, Longitude, LOCALTIMESTAMP AS created_at, LOCALTIMESTAMP AS updated_at
          FROM imports
        SQL
      end
    end

    def self.upd_location
      User.find_each do |user|
        sample_location = user.geo_datum.sample
        location = Location.find_or_initialize_by(user_id: user.id)
        location.update(latitude: sample_location.latitude, longitude: sample_location.longitude)
      end
    end
  end
end
