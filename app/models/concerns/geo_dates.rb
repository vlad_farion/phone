module GeoDates
  extend ActiveSupport::Concern
  included do
    belongs_to :user

    validates_presence_of :latitude, :longitude, :user_id
    validates :latitude, numericality: { greater_than_or_equal_to: -90, less_than_or_equal_to: 90 }
    validates :longitude, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }
  end

  def self.add_geo_log(data)
    GeoDatum.create!(latitude: data.latitude, longitude: data.longitude, user_id: data.user_id)
  end
end
