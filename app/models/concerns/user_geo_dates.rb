module UserGeoDates
  extend ActiveSupport::Concern
  included do
    has_one :location, dependent: :destroy

    accepts_nested_attributes_for :location

    def find_or_create_geo!(latitude, longitude)
      geo = Location.find_or_initialize_by(user_id: id)
      geo.update_attributes(latitude: latitude, longitude: longitude)
      geo.save
      geo
    end

    def nearest_location(args)
      Location.in_range(args, origin: location).where.not(user_id: id)
    end

    def near_location(distance)
      User.where(id: Location.within(distance, origin: location).where.not(user_id: id).pluck(:user_id))
      # Location.within(distance, origin: location).where.not(user_id: id)
    end

    def self.sympathies_location(user, distance)
      User.where(id: Location.within(distance, origin: user.location).pluck(:user_id))
    end
  end
end
