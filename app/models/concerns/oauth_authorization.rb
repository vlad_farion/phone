module OauthAuthorization
  extend ActiveSupport::Concern

  included do
    def self.build_user(application_id, provider)
      return respond_with_i18n_error('auth.errors.no_token') unless provider[:token].present?

      authorization = Authorization.find_by(uid: provider[:token].to_s, provider: provider[:name])
      user = authorization.present? ? User.find_by_id(authorization.user_id) : nil

      unless user
        password = Devise.friendly_token[0..10]
        user = User.new(email: "#{SecureRandom.hex(8)}@example.org",
                        password: password,
                        password_confirmation: password)
        user.save if user.valid?
        user.authorizations.create!(provider: provider[:name], uid: provider[:token].to_s) if user.persisted?
      end
      token = user.token.nil? ? user.doorkeeper_auth(application_id) : user.token
      if token
        if provider[:name] == 'doorkeeper'
          user.update_attribute(:confirmed_at, nil) if user.confirmed_at
          # SmsConfirmationJob.perform_later(user)
        else
          user.confirm! if user.confirmed_at.nil?
        end
      end

      {status: :success, body: token}
    end

    def doorkeeper_auth(application_id)
      Doorkeeper::AccessToken.create!(application_id: application_id, resource_owner_id: id)
    end

    def user_authorization(provider)
      authorizations.where(provider: provider)
    end

    def confirm!
      update!(confirmed_at: Time.current)
    end

    def respond_with_i18n_error(key)
      { status: :error, body: I18n.t(key) }
    end
  end
end
