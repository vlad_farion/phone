module SmsConfirmable
  extend ActiveSupport::Concern

  class ConfirmationSender
    def self.send_confirmation_to(user)
      verification_code = CodeGenerator.generate
      user.update(confirmation_token: verification_code, confirmation_sent_at: Time.current)
      # MessageSender.send_code(user.user_authorization('doorkeeper').take.uid, verification_code)
    end
  end

  class CodeGenerator
    def self.generate
      rand(100_000...999_999).to_s
    end
  end

  class MessageSender
    def self.send_code(phone_number, code)
      account_sid = Rails.application.secrets.twilio_account_sid
      auth_token  = Rails.application.secrets.twilio_auth_token
      client = Twilio::REST::Client.new(account_sid, auth_token)

      message = client.api.account.messages.create(
        from:  Rails.application.secrets.twilio_number,
        to:    "#{phone_number}",
        body:  code
      )

      message.status == 'queued'
    end
  end
end
