class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user

    alias_action :update, :destroy, to: :modify
    # alias_action :vote_plus, :vote_minus, :vote_cancel, to: :vote

    if user
      user_abilities
    else
      guest_abilities
    end
  end

  def guest_abilities
    # can :read, :all
    can :create, User
  end

  def user_abilities
    guest_abilities

    can :create, [Photo, Location, GeoDatum, UserProfile, Sympathy, Interest]

    can :modify, [Photo, Location, UserProfile, GeoDatum, Sympathy, Interest], user_id: @user.id

    can :read, [GeoDatum, Location], user_id: @user.id

    can :read, [Photo, UserProfile, User, Sympathy, Interest]

    can :me, User
  end
end
