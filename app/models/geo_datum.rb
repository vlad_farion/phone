class GeoDatum < ApplicationRecord
  include GeoDates
  include ImportGeo
  belongs_to :user

  after_create_commit :update_location

  def update_location
    geo = Location.find_or_initialize_by(user_id: user_id)
    geo.update_attributes(latitude: latitude, longitude: longitude)
  end
end
