class Interest < ApplicationRecord
  include PgSearch

  has_and_belongs_to_many :user_profiles

  validates_uniqueness_of :name

  pg_search_scope :find_by_name, against: %i(name), using: { tsearch: { prefix: true } }
end
