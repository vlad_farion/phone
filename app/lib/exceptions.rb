module Exceptions
  class UsersErrors < StandardError; end
  class UserNotConfirmed < UsersErrors; end
end
