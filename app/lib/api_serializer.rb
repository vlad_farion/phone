class ApiSerializer < ActiveModel::Serializer
  ActiveModel::Serializer.config.key_transform = :camel_lower
end
