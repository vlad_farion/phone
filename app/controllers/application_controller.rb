require 'application_responder'

class ApplicationController < ActionController::API
  before_action :set_locale
  self.responder = ApplicationResponder

  respond_to :json

  rescue_from CanCan::AccessDenied do
    render json: { error: I18n.t('ability.forbidden') }.to_json, status: :forbidden
  end

  private

  def set_locale
    if params[:locale] && I18n.available_locales.include?(params[:locale].to_sym)
      I18n.locale = params[:locale]
    end
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end
end
