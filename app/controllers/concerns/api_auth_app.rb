module ApiAuthApp
  extend ActiveSupport::Concern

  included do
    private

    def check_app
      application = Doorkeeper::Application.find_by(uid: params[:uid], secret: params[:secret])
      if application.present?
        @application = application
      else
        render_callback(I18n.t('api.errors.no_app'), 401)
      end
    end

    def render_callback(detail, status)
      render json: { detail: detail }, status: status
    end

    def render_errors(callback, status)
      render json: callback.to_json, status: status
    end

    # rubocop:disable Performance/HashEachMethods
    def normalize_keys!(val = params)
      if val.class == Array
        val.map { |v| normalize_keys! v }
      else
        if val.respond_to?(:keys)
          val.keys.each do |k|
            current_key_value = val[k]
            val.delete k
            val[k.to_s.underscore] = normalize_keys!(current_key_value)
          end
        end
        val
      end
      val
    end
    # rubocop:enable Performance/HashEachMethods

    def camelize_keys!(hash)
      ApiConventionsHelper::HashTransformer.new.camelize_keys(hash)
    end

    def snakecase_params!(hash)
      ApiConventionsHelper::HashTransformer.new.snakecase_params(hash)
    end
  end
end
