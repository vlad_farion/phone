class Api::V1::SympathiesController < Api::V1::BaseController
  before_action :doorkeeper_authorize!
  before_action :current_resource_owner

  authorize_resource class: Sympathy

  def index
    # binding.pry
    respond_with @users = User.matches_list(@current_resource_owner), each_serializer: Api::V1::UserSerializer
  end

  def create
    @follower = User.find(params[:follower_id])
    @sympathy = Sympathy.between(@current_resource_owner.id, @follower)

    if @sympathy.present?
      @sympathy.update!(accepted: true)
      render json: @follower, serializer: Api::V1::UserSerializer, status: 200
    else
      create_matches(@current_resource_owner, @follower)
    end
  end

  protected

  def create_matches(user, follower)
    @match = Sympathy.create!(user_id: user.id, follower_id: follower.id)
    render_callback(I18n.t('sympathy.added'), 201)
  end
end
