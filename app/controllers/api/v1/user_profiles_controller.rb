class Api::V1::UserProfilesController < Api::V1::BaseController
  before_action :doorkeeper_authorize!

  authorize_resource class: UserProfile

  def update
    @profile = @current_resource_owner.user_profile
    if @profile.update(profile_params)
      render json: @profile, serializer: Api::V1::UserProfileSerializer, status: 200
    else
      render_errors({ errors: @profile.errors.full_messages }, 422)
    end
  end

  def show
    @user = User.find(params[:user_id])
    respond_with @profile = @user.user_profile, serializer: Api::V1::UserProfileSerializer
  end

  private

  def profile_params
    params.require(:user).permit(:name, :gender, :city, :country, :sexual, :birthday)
  end
end
