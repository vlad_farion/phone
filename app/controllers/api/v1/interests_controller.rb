class Api::V1::InterestsController < Api::V1::BaseController
  USER_INTEREST_LIMIT = 10
  before_action :doorkeeper_authorize!
  before_action :current_resource_owner

  authorize_resource class: Interest

  def index
    @interests = Interest.all
    @interests = @interests.find_by_name(params[:query]) if params[:query]
    respond_with @interests, each_serializer: Api::V1::InterestSerializer
  end

  def create
    return render_errors({ errors: [I18n.t('api.errors.bad_params')] }, 422) if params[:interest].nil? || params[:interest][:name].nil?

    return render_errors({ errors: [I18n.t('api.errors.too_many_interests')] }, 409) if user_interests.count >= USER_INTEREST_LIMIT

    interest = Interest.find_or_create_by(name: interest_params[:name].downcase)
    user_interests << interest unless user_interests.exists?(interest.id)
    render json: interest, status: 201, each_serializer: Api::V1::InterestSerializer
  end

  def destroy
    respond_with user_interests.delete(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    render_errors({ errors: [I18n.t('api.errors.not_found')] }, 404)
  end

  protected

  def interest_params
    params.require(:interest).permit(:name)
  end

  def user_interests
    @current_resource_owner.user_profile.interests
  end
end
