class Api::V1::BaseController < ApplicationController
  include ApiConventionsHelper
  include ApiAuthApp
  before_action :normalize_keys!
  rescue_from Exceptions::UserNotConfirmed, with: :render_user_not_confirmed
  # protect_from_forgery unless: -> { request.format.json? }

  protected

  def current_resource_owner
    @current_resource_owner ||= User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token

    if @current_resource_owner && !@skip_user_confirmation &&
        @current_resource_owner.confirmed_at.nil?
      raise Exceptions::UserNotConfirmed
    end

    @current_resource_owner
  end

  def current_ability
    @ability ||= Ability.new(current_resource_owner)
  end

  def render_user_not_confirmed
    render_errors({ errors: [I18n.t('api.errors.user_not_confirmed')] }, 422)
  end
end
