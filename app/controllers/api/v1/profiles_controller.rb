class Api::V1::ProfilesController < Api::V1::BaseController
  before_action :doorkeeper_authorize!, except: %i[create]
  before_action :check_app, only: %i[create]

  authorize_resource class: User

  def show
    respond_with @user = User.find(params[:id]), serializer: Api::V1::UserSerializer
  end

  def me
    respond_with current_resource_owner, serializer: Api::V1::UserSerializer
  end

  def create
    return render_errors({ errors: [I18n.t('api.errors.bad_params')] }, 422) if params[:provider].nil?
    return render_errors({ errors: [I18n.t('api.errors.provider_dont_exist')] }, 422) unless ['doorkeeper', 'google', 'facebook'].include?(params[:provider][:name])

    response = User.build_user(@application.id, params[:provider])

    if response[:status] == :success
      render json: { access_token: response[:body].token }, status: 201
    else
      render_errors(response[:body], 422)
    end
  end

  def destroy
    @user = User.find(current_resource_owner.id)
    respond_with @user.destroy
  end

  private

  def invalid_login_attempt
    render json: { errors: t('api.errors.no_login') }, status: 401
  end

  def tokenize
    @access_token = @user.doorkeeper_auth(@application.id)
    render json: { access_token: @access_token.token }, status: 201
  end

  def user_params
    params.require(:user).permit(:email, :password, :provider, :uid)
  end
end
