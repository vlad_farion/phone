class Api::V1::SmsConfirmationsController < Api::V1::BaseController
  before_action :skip_user_confirmation
  before_action :doorkeeper_authorize!
  before_action :current_resource_owner

  def create
    return render_errors({ error: I18n.t('confirmation_token.errors.already_confirmed')}, 409) if current_resource_owner.confirmed_at

    SmsConfirmationJob.perform_later @current_resource_owner
    render_callback(I18n.t('confirmation_token.success.sended'), 200)
  end

  def update
    return render_errors({ error: I18n.t('confirmation_token.errors.already_confirmed')}, 409) if current_resource_owner.confirmed_at

    if params[:confirmation_code] == '111111'
    # if params[:confirmation_code] == @current_resource_owner.confirmation_token
      @current_resource_owner.confirm!
      render_callback(I18n.t('confirmation_token.success.confirmed'), 201)
    else
      render_errors({ error: I18n.t('confirmation_token.errors.no_valid') }, 404)
    end
  end

  private

  def skip_user_confirmation
    @skip_user_confirmation = true
  end
end
