class Api::V1::UserSearchesController < Api::V1::BaseController
  before_action :doorkeeper_authorize!
  before_action :current_resource_owner

  # authorize_resource class: [UserProfile, User, Location, Photo]
  # load_and_authorize_resource class: [UserProfile, User, Location, Photo, GeoDatum]

  def near_location
    if @current_resource_owner.location.present?
      @users = @current_resource_owner.near_location(params[:distance])
      render json: @users, each_serializer: Api::V1::UserSerializer
    else
      render_errors(I18n.t('api.errors.no_location'), 422)
    end
  end
end
