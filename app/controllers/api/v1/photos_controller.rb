class Api::V1::PhotosController < Api::V1::BaseController
  before_action :doorkeeper_authorize!
  before_action :current_resource_owner
  before_action :load_photo, only: %i(show destroy)

  respond_to :json
  authorize_resource class: Photo

  def index
    respond_with @photos = @current_resource_owner.photos
  end

  def show
    respond_with @photo
  end

  def create
    return render_errors({ errors: [I18n.t('api.errors.bad_params')] }, 422) unless params[:photo].present?
    @photo = @current_resource_owner.photos.create(photo_params)
    if @photo.persisted?
      render json: @photo, status: 201
    else
      render_errors({ errors: @photo.errors.full_messages }, 422)
    end
  end

  def destroy
    # TODO check if photo belongs to current_resource_owner
    respond_with @photo.destroy
  end

  protected

  def load_photo
    @photo = Photo.find(params[:id])
  end

  def photo_params
    params.require(:photo).permit(:image)
  end
end
