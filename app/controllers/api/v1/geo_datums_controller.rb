class Api::V1::GeoDatumsController < Api::V1::BaseController
  before_action :doorkeeper_authorize!
  before_action :current_resource_owner

  authorize_resource class: GeoDatum

  def create
    @data = @current_resource_owner.geo_datum.create(geo_params.merge(user_id: @current_resource_owner.id))
    if @data.persisted?
      render json: @data, status: 201, each_serializer: Api::V1::GeoDatumSerializer
    else
      render_errors({ errors: @data.errors.full_messages }, 422)
    end
  end

  def index
    respond_with @data = @current_resource_owner.geo_datum
  end

  protected

  def geo_params
    params.require(:geo_data).permit(:latitude, :longitude)
  end
end
