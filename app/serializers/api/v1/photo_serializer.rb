class Api::V1::PhotoSerializer < ApiSerializer
  attributes :id, :user_id, :image_original, :image_medium, :image_small, :image_thumb

  def image_original
    ActionController::Base.helpers.asset_url(object.image.url)
  end

  def image_small
    ActionController::Base.helpers.asset_url(object.image.url(:small))
  end

  def image_medium
    ActionController::Base.helpers.asset_url(object.image.url(:medium))
  end

  def image_thumb
    ActionController::Base.helpers.asset_url(object.image.url(:thumb))
  end
end
