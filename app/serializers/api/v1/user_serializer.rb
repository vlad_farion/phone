class Api::V1::UserSerializer < ApiSerializer
  attributes :id, :created_at

  has_one :user_profile
  has_one :location
  has_many :photos

  def created_at
    object.created_at.iso8601
  end
end
