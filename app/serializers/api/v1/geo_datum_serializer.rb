class Api::V1::GeoDatumSerializer < ApiSerializer
  attributes :user_id, :latitude, :longitude, :created_at

  def created_at
    object.created_at.iso8601
  end
end
