class Api::V1::UserProfileSerializer < ApiSerializer
  attributes :name, :gender, :birthday, :city, :country, :sexual, :interests

  def interests
    object.interests.select(:id, :name)
  end

  def birthday
    birthday = object.try(:birthday)
    birthday.present? ? birthday.iso8601 : nil
  end
end
