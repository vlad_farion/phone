class Api::V1::LocationSerializer < ApiSerializer
  attributes :latitude, :longitude, :updated_at

  def updated_at
    object.updated_at.iso8601
  end
end
