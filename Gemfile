source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'
gem 'rails', '5.1.4'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem 'active_model_serializers'
gem 'aws-sdk', '2.10.55'
gem 'cancancan', '2.1.2'
gem 'devise', '4.3.0'
gem 'devise-i18n', '1.4.0'
gem 'doorkeeper'
gem 'geokit-rails'
gem 'haversine', '0.3.2'
gem 'i18n', '0.9.1'
gem 'oj'
gem 'oj_mimic_json'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'paperclip', '5.1.0'
gem 'paperclip-compression'
gem 'rack-cors'
gem 'rails-erd', require: false
gem 'rails-i18n', '5.0.4'
gem 'redis-namespace', '1.6.0'
gem 'redis-rails', '5.0.2'
gem 'rspec', '3.7.0'
gem 'sidekiq'
gem 'sidekiq-status', '1.0.0'
gem 'twilio-ruby', '5.6.2'
gem 'whenever', '0.10.0', require: false
gem 'pg_search'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'childprocess'
  gem 'factory_bot_rails', '4.8.2'
  gem 'faker'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'pry-rescue'
  gem 'pry-stack_explorer'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'timecop'
  gem 'webmock'
end

group :development do
  gem 'bullet', '5.7.0'
  gem 'capistrano', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-linked-files', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-rake', require: false
  gem 'capistrano-rbenv', require: false
  gem 'capistrano-sidekiq', require: false
  gem 'capistrano3-puma', github: 'seuros/capistrano-puma', require: false
  gem 'httparty'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_cleaner', '1.6.2'
  gem 'fuubar', '2.2.0'
  gem 'json_spec', '1.1.5'
  gem 'launchy', '2.4.3'
  gem 'rack_session_access', '0.1.1'
  gem 'shoulda-matchers', '3.1.2'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
