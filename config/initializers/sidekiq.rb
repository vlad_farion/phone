require 'sidekiq'
require 'sidekiq/web'

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [Rails.application.secrets.sidekiq_admin_user,
                       Rails.application.secrets.sidekiq_admin_password]
end

Sidekiq::Web.set :session_secret, Rails.application.secrets.secret_key_base

Sidekiq.configure_client do |config|
  config.redis = { url: Rails.application.secrets.redis_url, size: 3 }
end

Sidekiq.configure_server do |config|
  config.redis = { url: Rails.application.secrets.redis_url, size: 27 }
end
