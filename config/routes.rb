require 'sidekiq/web'
Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users
  Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
    [user, password] == [Rails.application.secrets[:sidekiq_name], Rails.application.secrets[:sidekiq_passw]]
  end

  scope '(:locale)', locale: /#{I18n.available_locales.join('|')}/ do
    mount Sidekiq::Web => '/sidekiq'
    namespace :api, defaults: { format: :json } do
      namespace :v1 do
        resources :profiles, only: %w(create show)
        match :me, to: 'profiles#me', as: 'me', via: :get
        resources :photos, only: %i(create show index destroy)
        resources :geo_datums, only: %i(create index)
        resource :user_profiles, only: %i(update show)
        resource :user_searches, only: %i(show) do
          get :near_location
        end
        resources :sympathies, only: %i(create index)
        resource :sms_confirmations, only: %i(create update)
        resources :sympathies, only: %i(create index)
        resources :interests, only: %i(index create destroy)
      end
    end
  end
end
